//
//  ListViewController.swift
//  SampleUITable
//
//  Created by Buzz on 15/8/17.
//  Copyright © 2017 Imtayaz Khan. All rights reserved.
//

import UIKit

class ListViewController: UIViewController {
    
    let jsonData = JSONDataModel.dataFromURL()

    @IBOutlet weak var tableView: UITableView!
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(ListViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        refreshControl.tintColor = .gray
        
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.estimatedRowHeight = 44.0 ;
        self.tableView.rowHeight = UITableViewAutomaticDimension;
        self.tableView.addSubview(self.refreshControl)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        NotificationCenter.default.addObserver(forName: .UIContentSizeCategoryDidChange, object: .none, queue: OperationQueue.main) { [weak self] _ in
            self?.tableView.reloadData()
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? DetailsViewController,
            let indexPath = tableView.indexPathForSelectedRow {
            destination.selectedJSON = jsonData[indexPath.row]
        }
    }
    
    
    func handleRefresh(_ refreshControl: UIRefreshControl) {
        // Do some reloading of data and update the table view's data source
        // Fetch more objects from a web service, for example...
        
        // Simply adding an object to the data source for this example
        
        self.tableView.reloadData()
        refreshControl.endRefreshing()
    }

}

extension ListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return jsonData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! JSONTableViewCell
        let jData = jsonData[indexPath.row]
        cell.titleLabel?.text = jData.title
        cell.descriptionLabel?.text = jData.description
        cell.titleLabel?.textColor = .blue
        cell.descriptionLabel?.textColor = .black
        cell.imageImageView.downloadedFrom(link: jData.image)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
