//
//  JSONDataModel.swift
//  SampleUITable
//
//  Created by Buzz on 15/8/17.
//  Copyright © 2017 Imtayaz Khan. All rights reserved.
//

import UIKit

struct JSONDataModel {
    let title: String
    let description: String
    let image: String
    
    init(title: String, description: String, image: String) {
        self.title = title
        self.description = description
        self.image = image
    }
    static func dataFromURL()  -> [JSONDataModel] {
        var sampleData = [JSONDataModel]()
        
        guard let url = URL(string: "https://api.myjson.com/bins/m47pd") else {
            return sampleData
        }
        
        do {
            let data = try Data(contentsOf: url)
            guard let rootObject = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String : Any]  else {
                return sampleData
            }
            
            guard let rowsObjects = rootObject["rows"] as? [[String: AnyObject]] else {
                return sampleData
            }
            var jTitle:String? = nil
            var jDescription: String? = nil
            var jImage:String? = nil
            for rowsObject in rowsObjects {
                if let title = rowsObject["title"] as? String {
                    jTitle = title
                }
                if let description = rowsObject["description"]  as? String {
                    jDescription = description
                }
                
                if let image = rowsObject["imageHref"] as? String {
                    jImage = image
                }
                let jsonObj = JSONDataModel(title: jTitle!, description: jDescription!, image: jImage!)
                sampleData.append(jsonObj)
                
            }
        } catch {
            return sampleData
        }
 
        
        return sampleData
    }
}
