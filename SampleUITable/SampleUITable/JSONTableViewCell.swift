//
//  JSONTableViewCell.swift
//  SampleUITable
//
//  Created by Buzz on 15/8/17.
//  Copyright © 2017 Imtayaz Khan. All rights reserved.
//

import UIKit

class JSONTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var imageImageView: UIImageView!

    
}
